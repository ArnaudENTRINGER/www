---
title: "MORET"
permalink: /docs/moret/
---

The MORET software, currently at its 5.D.1 version –available upon request at the OECD/NEA Data Bank-, is a simulation tool that solves the neutron transport equation using Monte Carlo methods. Users can model complex 3D geometric configurations thanks to a special feature (« modules »). The code supports a large variety of nuclear data libraries and offers the user the possibility to select the most suitable simulation method for his configuration, as well as to define its own tallies. Its primary goal is to fulfill the needs of nuclear criticality safety assessment but it can also handle reactor physics studies. It is fully compatible with the in-house VESTA Monte Carlo depletion code.

**Modelling the geometry of objects**
The description of the geometry relies on combinatorial geometry for which user has to create volumes that will contain the materials. Operators may be added to describe the combinatorial properties of the volumes. The code gives the possibility to define the geometry as a composition of several regions, by using basic building blocks. These parts of the whole geometry can be embedded one in another. A geometry-plotting function is available to display the modeling. **

**Description of isotopes cross sections**
The MORET code offers two calculation schemes for the energy treatment of cross sections : the multi-group approach (with direct use of macroscopic cross section sets, which result from preliminary cell code calculations), and continuous energy calculations. Technics based on the Iterated Fission Probability allow for fast and reliable perturbations and sensitivities calculations, and for effective kinetics parameters estimation.

**Neutron simulation and tracking**
Besides the classical power iteration technic (iteration over neutrons generations and population control), MORET embeds several tracking capabilities and neutron source sampling options to tackle two main issues: forcing the neutrons to visit all fissile volumes -to reduce the risks of weak coupling-, and accelerating the source convergence. The sampling methods are the analogue method, the stratified sampling, the fission matrix method, the importance method, the oversampling method, the superhistory powering and the Wielandt method.
 
**Verification and validation**
MORET benefits from a large validation database that comprises more than 2300 critical experiments. This database covers a broad range of configurations in terms of fissile environments, moderators, reflectors and neutron spectra (to validate different materials and cover a large range of moderation ratios).

**Future and main up-coming developments**
MORET is currently undergoing a series of development aiming at meeting the needs of reactor physics codes (e.g. simulating power transient using a coupling with thermal-hydraulic codes), at integrating correlated physics capabilities (coupling with fission sampling codes), or at using the geometry tracking capabilities of other Monte Carlo transport codes.

![JH]({{ site.baseurl }}/docs/JH.png)





