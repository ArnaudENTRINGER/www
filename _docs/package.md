---
title: Package
permalink: /docs/package/
---

## Requirements

  * User side: Windows or Linux computer with Java 8,
  * Server side: Linux 32/64.


## Entry point

The graphical workbench LATEC embeds both CAD features for 3D modelling and parametric calculation features dedicated to criticality safety studies:

  * the CAD model relies on a generic descriptive syntax, which is "compiled" in true code syntax (APOLLO2, TRIPOLI-4(R) or MORET 5) just before the calculation. This allows providing the same LATEC ergonomics for any CRISTAL code,
  * the client-server architecture distributes parallel calculations on a remote server/cluster, so the user can perform the design on its desktop computer (Windows or Linux) without computer codes installed (APOLLO2, TRIPOLI-4(R), MORET 5).

<hr/>
<center>
<iframe width="768" height="431" src="https://www.youtube.com/embed/WPgZHX0J_Vk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</center>
<center>
<iframe width="768" height="431" src="https://www.youtube.com/embed/JfcDD4dSFNU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</center>

## Content

The various components of the CRISTAL V2.0 package are the following:

  * [CEAV5.1.2](/docs/ceav5) library for APOLLO2 code (CEA)
  * [CEAV5.1.2](/docs/ceav5) library for TRIPOLI-4(R) code (CEA)
  * Complement for CEA V5.1.2 library for APOLLO2 code dedicated to CRISTAL V2.0.2 (18 nuclides at 214 K), (CEA)
  * APROC2.8-3.C procedures library (CEA)
  * APROC_CRISTAL V2.0 specific procedures library (CEA)
  * [APOLLO2.8-3.C computer code](/docs/apollo), (CEA)
  * [TRIPOLI-4(R) Version 8.1 computer code](/docs/tripoli), (CEA)
  * [LATEC V1.4 workbench](/docs/latec), (IRSN)
  * [MORET 5.B.1 computer code](/docs/moret) (IRSN)
  * [Qualification database](/docs/qualification) (CEA & IRSN)

