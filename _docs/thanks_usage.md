---
title: "Thanks !"
permalink: /docs/thanks_usage/
---

Thank you for using CRISTAL.
Do not hesitate to report issues and feedback on https://www.cristal-package.org/docs/support/ .
